package root.services;

import java.io.StringReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.json.Json;
import javax.json.JsonObject;

@Path("/")
public class Dolar {
    @Context
    private HttpHeaders header;
    @Context
    HttpServletRequest request;
    
    @GET
    @Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON,MediaType.TEXT_PLAIN})
    public Response getDolar(){
        Client client=ClientBuilder.newClient();
        //String dolar=request.getHeader("X-Forwarded-For");
        LocalDate localDate=LocalDate.now();
        String fecha=DateTimeFormatter.ofPattern("yyyy/MM/dd").format(localDate);
        //String fechaSplit[]=fecha.split("/");
        //int anio=Integer.parseInt(fechaSplit[0]);
        //int mes=Integer.parseInt(fechaSplit[1]);
        //int dia=Integer.parseInt(fechaSplit[2]);
        String url="https://api.sbif.cl/api-sbifv3/recursos_api/dolar?formato=JSON&apikey=2e56744265d71f6d1dcb422373511cbda48e566f";
        Response respApiDol=client.target(url).request().get();
        String dataResp=respApiDol.readEntity(String.class);
        if (dataResp.contains("CodigoHTTP")){
            System.out.print("No hay datos para hoy");
            LocalDate ayer=localDate.minusDays(1);
            fecha=DateTimeFormatter.ofPattern("yyyy/MM/dd").format(ayer);
            String fechaSplit[]=fecha.split("/");
            String anio=fechaSplit[0];
            String mes=fechaSplit[1];
            String dia=fechaSplit[2];
            String urlayer="https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+anio+"/"+mes+"/"+dia+"/?formato=JSON&apikey=2e56744265d71f6d1dcb422373511cbda48e566f";
            respApiDol=client.target(urlayer).request().get();
            dataResp=respApiDol.readEntity(String.class);
            if (dataResp.contains("CodigoHTTP")){
                System.out.print("No hay datos para ayer");
                LocalDate anteayer=localDate.minusDays(2);
                fecha=DateTimeFormatter.ofPattern("yyyy/MM/dd").format(anteayer);
                String fechaSplit2[]=fecha.split("/");
                String anio2=fechaSplit2[0];
                String mes2=fechaSplit2[1];
                String dia2=fechaSplit2[2];
                String urlanteayer="https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+anio2+"/"+mes2+"/dias/"+dia2+"/?formato=JSON&apikey=2e56744265d71f6d1dcb422373511cbda48e566f";
                respApiDol=client.target(urlanteayer).request().get();
                dataResp=respApiDol.readEntity(String.class);
            }
        }
        return Response.ok("{\"Valor dolar entregado por SBIF\":"+fecha+": "+dataResp+" }").build();  
    }
    @GET
    @Path("/dolar/{fecha}")
    @Produces({MediaType.TEXT_HTML,MediaType.APPLICATION_JSON,MediaType.TEXT_PLAIN})
    public Response getDolarParametro(@PathParam("fecha") String fecha){
        //JsonObject j=Json.createReader(new StringReader(body)).readObject();
        Client client=ClientBuilder.newClient();
        String fechas[]=fecha.split("-");
        String fecha2=fechas[0]+"/"+fechas[1]+"/dias/"+fechas[2];
        String url="https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+fecha2+"/?formato=JSON&apikey=2e56744265d71f6d1dcb422373511cbda48e566f";
        Response respApi=client.target(url).request().get();
        String datosRespuesta=respApi.readEntity(String.class);
        return Response.ok("{\"Valor dolar entregado por SBIF\":"+fecha+": "+datosRespuesta+" }").build();
    }
}

